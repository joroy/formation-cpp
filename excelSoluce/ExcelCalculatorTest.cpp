#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "ExcelCalculator.h"
#include "ExcelFileMock.h"

#include <QString>

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::_;

using namespace kata;

namespace {

class ExcelCalculatorTest : public ::testing::Test {
protected:
    ExcelCalculatorTest()
        : calculator(file)
    {
    }
    virtual ~ExcelCalculatorTest(){}

    NiceMock<ExcelFileMock> file;
    ExcelCalculator calculator;
};

TEST_F(ExcelCalculatorTest, givenX_whenY_thenZ)
{
}

}  // namespace

